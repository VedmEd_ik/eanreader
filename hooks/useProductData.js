import { useEffect, useState } from 'react';
import { getDatabase, ref, get } from 'firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { appFirebase } from '../firebase';
import { Alert } from 'react-native';
import NetInfo from '@react-native-community/netinfo';

const useProductData = () => {
	const [products, setProducts] = useState(null);
	const [isConnected, setIsConnected] = useState(true);

	const fetchLocalData = async () => {
		try {
			// Перевірка, чи є дані в AsyncStorage
			const localData = await AsyncStorage.getItem('ean-reader-verest-products');
			if (localData) {
				setProducts(JSON.parse(localData)); // Встановлення даних з AsyncStorage
			} else {
				console.log("No data from server available");
				Alert.alert("Помилка", "Локальна база продукції відсутня!")
			}
		} catch (error) {
			console.error(error);
			Alert.alert("Помилка", "Локальна база продукції відсутня!")
		}
	};

	useEffect(() => {
		const unsubscribe = NetInfo.addEventListener(state => {
			setIsConnected(state.isConnected);
		});

		return () => unsubscribe();
	}, []);

	useEffect(() => {
		if (isConnected) {
			const dbRef = ref(getDatabase(appFirebase)); // Створення посилання на базу даних

			get(dbRef).then((snapshot) => { // Отримання даних з бази даних
				if (snapshot.exists()) {
					const data = snapshot.val();
					setProducts(data); // Встановлення даних товарів
					AsyncStorage.setItem('ean-reader-verest-products', JSON.stringify(data)); // Запис даних в AsyncStorage
				} else {
					console.log("No data from server available");
					Alert.alert("Помилка сервера", "Не вдалося отримати дані від сервера. Перевірка локальної бази продукції")
					fetchLocalData();
				}
			}).catch((error) => {
				console.error(error);
				Alert.alert("Помилка сервера", "Не вдалося отримати дані від сервера. Перевірка локальної бази продукції")
				fetchLocalData();
			});
		} else {
			fetchLocalData();
		}



	}, [isConnected]);

	return products;
};

export default useProductData;
