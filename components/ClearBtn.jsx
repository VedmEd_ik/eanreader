import React from 'react';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';

const ClearBtnView = ({ clear }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={clear} style={styles.buttonStyle}>
        <Text style={styles.buttonText}>Очистити</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  buttonStyle: {
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 12,
    paddingRight: 12,
    borderWidth: 2,
    borderColor: '#87C4FF',
    borderRadius: 5,
    backgroundColor: '#E0F4FF',
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    color: '#39A7FF',
    fontWeight: '700',
  },
});

export default ClearBtnView;
