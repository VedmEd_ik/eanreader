import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button, Image, TouchableOpacity } from 'react-native';
import { Camera } from 'expo-camera';

export default function BarCodeScan({ getEan, closeScan }) {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = ({ data }) => {
    setScanned(true);
    getEan(data);
  };

  if (hasPermission === null) {
    return <Text>Запит на доступ до камери</Text>;
  }
  if (hasPermission === false) {
    return <Text>Немає доступу до камери</Text>;
  }

  return (
    <View style={styles.container}>
      <Camera onBarCodeScanned={scanned ? undefined : handleBarCodeScanned} style={{ flex: 1 }} />
      <TouchableOpacity onPress={() => closeScan()} style={styles.closeIconWrapp}>
        <Image source={require('../assets/close_icon.png')} style={styles.closeIcon} />
      </TouchableOpacity>
      {scanned && (
        <Button title={'Натисніть, щоб знову сканувати'} onPress={() => setScanned(false)} />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    width: 300,
    height: 200,
    position: 'relative',
  },
  closeIconWrapp: {
    position: 'absolute',
    right: 10,
    top: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    padding: 5,
    borderRadius: 5,
  },
  closeIcon: {
    height: 20,
    width: 20,
  },
});
