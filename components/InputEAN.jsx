import {useEffect, useRef, useState} from 'react';
import {TextInput, View, Text, StyleSheet} from 'react-native';

const InputEAN = ({getEan, inputValue}) => {
  const inputRef = useRef();
  const [isFocused, setIsFocused] = useState(false);

  useEffect(() => {
    // автофокус на поле введення штрихкоду, якщо це поле пусте
    !inputValue && inputRef.current.focus();
  }, [inputValue]);

  return (
    <View style={styles.inputContainer}>
      <Text>Введіть штрих-код</Text>
      <TextInput
        style={[
          isFocused && {
            borderWidth: 3,
          },
          styles.input,
        ]}
        onChangeText={getEan}
        value={inputValue}
        ref={inputRef}
        onFocus={() => setIsFocused(true)}
        onBlur={() => setIsFocused(false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  input: {
    width: 250,
    padding: 10,
    borderWidth: 1,
    borderColor: '#87c4ff',
    borderRadius: 5,
  },
});

export default InputEAN;
