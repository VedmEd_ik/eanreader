import { View, Text } from 'react-native';

const EANUndefined = ({}) => {
  return (
    <View>
      <Text>Штрих-код не знайдено!</Text>
    </View>
  );
};

export default EANUndefined;
