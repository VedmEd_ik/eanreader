// Функція, яка обрізає рядок до заданої кількості символів (за замовчуванням 8 смиволів)
export const cutCode = (EAN13, countChar = 8) => {
	return EAN13 && EAN13.substring(0, countChar);
}

// Функція, яка перевіряє чи товар ваговий чи поштучний. Якщо ваговий, то повертає true, якщо ні - то false
export const checkEanIsWeight = (ean, firstChar = '48') => {
	return ean && (ean.substring(0, firstChar.length) === firstChar) ? false : true;
}

// Функція, яка перевіряє наявність штрихкоду в базі даних та повертає знайдені значення
export const filterData = (data, ean) => {
	return data.filter(item => {
		return checkEanIsWeight(item.EAN) ? cutCode(item.EAN) === ean : (item.EAN) === ean;
	});
}

// Функція, яка повертає валідний (обрізаний або не обрізаний) штрихкод
export const returnValidEan = (ean) => {
	return checkEanIsWeight(ean) ? cutCode(ean) : ean;
}

// Функція, яка допомагає зробити формат даних для бази data.json
export const makeImages = (data) => {
	const images = data.reduce((imagesObj, item) => {
		if (item.IMG_PATH && item.EAN) {
			if (checkEanIsWeight(item.EAN)) {
				imagesObj[cutCode(item.EAN)] = `./${item.IMG_PATH}`
			} else {
				imagesObj[item.EAN] = `./${item.IMG_PATH}`
			}
		}
		return imagesObj;
	}, {});
	console.log(images);
};
