import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image
} from 'react-native';
import { Image as ExpoImageCache } from 'react-native-expo-image-cache';

import InputEAN from './components/InputEAN';
import ClearBtnView from './components/ClearBtn';
import EANUndefined from './components/EANUndefined';
import {
  checkEanIsWeight,
  cutCode,
  filterData,
} from './utils/functions';
import useProductData from './hooks/useProductData';
import BarCodeScan from './components/BarcodeScanner';

const statusBarHeight = StatusBar.currentHeight || 30;

function App() {
  const [isError, setIsError] = useState(true); // Стан для визначення наявності помилки
  const [inputValue, setInputValue] = useState(''); // Стан для збереження значення, введеного користувачем
  const [filteredProducts, setFilteredProducts] = useState(); // Стан для збереження відфільтрованих товарів
  const [isScan, setIsScan] = useState(false);

  const products = useProductData();

  // Функція, яка отримує введений користувачем штрихкод, перевіряє його чи належить він ваговому товару і якщо так, то обрізає його до 8 символів
  const getEanFromInput = ean => {
    setInputValue(ean.trim())
  };

  useEffect(() => {
    if (products && inputValue && inputValue.length > 7) { // Перевірка, чи є дані товарів і чи є введений користувачем штрихкод
      const filteredData = checkEanIsWeight(inputValue) // Перевірка, чи є штрихкод ваговим
        ? filterData(products, cutCode(inputValue)) // Фільтрація даних товарів
        : filterData(products, inputValue)

      if (filteredData.length > 0) {
        setTimeout(() => {
          setFilteredProducts(filteredData); // Встановлення відфільтрованих товарів
          setIsError(false) // Встановлення помилки як false
        }, 500)
      } else {
        setIsError(true); // Встановлення помилки як true
        setFilteredProducts(false);
      }
    } else if (!inputValue) {
      setIsError(false); // Встановлення помилки як false
    } else {
      setIsError(true); // Встановлення помилки як true
      setFilteredProducts(false);
    }
  }, [inputValue]);

  useEffect(() => {
    !isError && setInputValue(''); // Очищення значення введеного користувачем, якщо помилка не виникла
  }, [isError]);

  useEffect(() => {
    filteredProducts && setInputValue('') // Очищення значення введеного користувачем, якщо є відфільтровані товари
  }, [filteredProducts]);

  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.container}>

          <View style={styles.wrapperButtonScan}>
            <TouchableOpacity onPress={() => setIsScan(true)} >
              <Image source={require('./assets/barcode-scanner.png')} style={styles.buttonScanImg} />
            </TouchableOpacity>
          </View>

          {isScan && <BarCodeScan getEan={getEanFromInput} closeScan={() => setIsScan(false)} />}

          <InputEAN getEan={getEanFromInput} inputValue={inputValue} />



          <ClearBtnView
            clear={() => {
              setInputValue(''); // Функція очищення значення введеного користувачем
              setFilteredProducts(false); // Функція очищення відфільтрованих товарів
            }}
          />

          {!isError ? ( // Перевірка наявності помилки
            filteredProducts && (
              <FlatList // Список відфільтрованих товарів
                horizontal
                data={filteredProducts}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View style={styles.productContainer}>
                    <Text style={[styles.productTitle, styles.text]}>{item.NAME}</Text>
                    <Text style={[styles.text]}>PLU: {item.NUM}</Text>
                    <Text style={[styles.text]}>EAN: {item.EAN}</Text>
                    {item.F_GROUPPLU && <Text style={[styles.text]}>Клієнт: {item.F_GROUPPLU}</Text>}
                    {item.IMG_PATH !== '' && (
                      <ExpoImageCache // Зображення товару
                        style={styles.productImage}
                        uri={item.IMG_PATH}
                        key={item.IMG_PATH} // Унікальний ключ кешування
                        preview={{ uri: item.THUMBNAIL_PATH }}
                        tint="light"
                      />
                    )}
                  </View>
                )}
              />
            )
          ) : (
            <EANUndefined /> // Компонент, що відображається у випадку відсутності товарів по штрихкоду
          )}
        </View>
      </ScrollView>
    </SafeAreaView >
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: statusBarHeight + 30,
  },
  scrollView: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    gap: 5,
  },
  productContainer: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 15,
    margin: 10,
    width: 300,
    height: 350,
    justifyContent: 'flex-start',
    alignItems: 'center',
    elevation: 5,
    borderColor: '#39A7FF',
    borderWidth: 1,
  },
  productTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  productImage: {
    flex: 1,
    width: `100%`,
    height: `100%`,
    resizeMode: 'cover',
    marginBottom: 10,
    borderRadius: 10,
  },
  text: {
    color: 'black'
  },
  wrapperButtonScan: {
    width: '100%',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-end'
  },
  buttonScanImg: {
    width: 50,
    height: 40,
    marginRight: 15,
    marginBottom: 15,
  }
});

export default App;
