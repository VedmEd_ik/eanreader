// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: "AIzaSyA4pCqEp2Yo-6nJIFo3cx_Bbkzr6keA9Ek",
	authDomain: "verestean-21ef1.firebaseapp.com",
	databaseURL: "https://verestean-21ef1-default-rtdb.europe-west1.firebasedatabase.app",
	projectId: "verestean-21ef1",
	storageBucket: "verestean-21ef1.appspot.com",
	messagingSenderId: "982389801954",
	appId: "1:982389801954:web:0f97e9f0b981a1fc026fbb"
};

// Initialize Firebase
export const appFirebase = initializeApp(firebaseConfig);